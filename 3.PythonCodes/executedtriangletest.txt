def judge_x(a,b,c):
    """
>>> judge_x(2,2,2)
equilateral
>>> judge_x(4,4,2)
isoceles
>>> judge_x(1,2,3)
scalene
judge_x(2,2,2)
equilateral
>>> judge_x(4,4,2)
isoceles
>>> judge_x(1,2,3)
scalene
judge_x(2,2,2)
equilateral
>>> judge_x(4,4,2)
isoceles
>>> judge_x(1,2,3)
scalene
judge_x(2,2,2)
equilateral
>>> judge_x(4,4,2)
isoceles
>>> judge_x(1,2,3)
scalene
judge_x(2,2,2)
equilateral
>>> judge_x(4,4,2)
isoceles
>>> judge_x(1,2,3)
scalene
"""
    if a == b == c:
     print( "equilateral")
    elif (a == b) or (b == c):
     print( "isoceles")
    else:
     print("scalene")

'''def classify_triangle(a, b, c):
  if a == b == c:
    return 'equilateral'
  elif (a == b) or (b == c):
    return 'isoceles'
  else:
    return 'scalene' '''

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=True) 
