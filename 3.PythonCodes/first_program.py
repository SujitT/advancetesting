def judge_x(x):
    """
>>> judge_x(1)
OK
>>> judge_x(4)
OK
>>> judge_x(-6)
Small
"""
    if 1 <= x <= 10:
        print("OK")
    elif x <= -5:
        print("Small")
    else:
        print("NOK")


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
    




##i = 15
##judge_x(i)

